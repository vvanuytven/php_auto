<?php

namespace CarsGallery\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BlogController extends Controller
{
    public $layout = 'layouts.default';
    public function indexAction()
    {
        return $this->render('CarsGalleryBlogBundle:Blog:Home.html.twig');
    }
    public function GalleryAction()
    {
        return $this->render('CarsGalleryBlogBundle:Blog:Gallery.html.twig');
    }
    public function ForumAction()
    {
        return $this->render('CarsGalleryBlogBundle:Blog:Forum.html.twig');
    }
    public function ContactAction()
    {
        return $this->render('CarsGalleryBlogBundle:Blog:Contact.html.twig');
    }
    
}

?>
