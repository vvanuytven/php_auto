<?php

/* CarsGalleryBlogBundle:Blog:Gallery.html.twig */
class __TwigTemplate_060868afb34c21c4c06a222c7a7c928d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
<title>Cars Gallery</title>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">
";
        // line 5
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "e0d0ea0_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e0d0ea0_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/e0d0ea0_Main_1.css");
            // line 8
            echo "<link type=\"text/css\" rel=\"stylesheet\" media=\"all\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
";
        } else {
            // asset "e0d0ea0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_e0d0ea0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/e0d0ea0.css");
            echo "<link type=\"text/css\" rel=\"stylesheet\" media=\"all\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
";
        }
        unset($context["asset_url"]);
        // line 10
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "5890f52_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_5890f52_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/5890f52_main_1.js");
            // line 11
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    <script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js\"></script>
    <script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js\"></script>
";
        } else {
            // asset "5890f52"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_5890f52") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/5890f52.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    <script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js\"></script>
    <script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js\"></script>
";
        }
        unset($context["asset_url"]);
        // line 15
        echo "</head>

<body class=\"bgimage\" alt=\"\" onload=\"Slider();\">
<!-- Save for Web Slices (MyPsd.psd) -->
<center>
            <div name=\"header\">
                <div name=\"theader\">
                            <img src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/theader_gauche.jpg"), "html", null, true);
        echo "\"
                            ><img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/theader_rss.jpg"), "html", null, true);
        echo "\"
                            ><img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/theader_droit.jpg"), "html", null, true);
        echo "\"
                            ><br>
                </div>

                <div name=\"header\" class=\"header\">
                            <img src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/header_top_gauche.jpg"), "html", null, true);
        echo "\" 
                            ><img src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/header_top_Logo.jpg"), "html", null, true);
        echo "\"
                            ><img src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/header_top_centre.jpg"), "html", null, true);
        echo "\"
                            ><form
                            ><input class=\"searchBox\" type=\"text\" value=\"Search your car !! \" onclick=\"this.value=''\" onmouseout=\"this.value='Search your car!!'\"
                            ><input class=\"searchBtn\" type=\"submit\" value=\"\"
                            ><img src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/header_top_droit.jpg"), "html", null, true);
        echo "\"</form><br>

                </div>
                <div name=\"bheader\">
                            <img class=\"header_bas_Gauche\"src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/header_bas_gauche.jpg"), "html", null, true);
        echo "\"
                            ><li class=\"Home\"><a class=\"Home\" href=\"Home\">
                                        <img src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Home.jpg"), "html", null, true);
        echo "\" 
                                                accesskey=\"\" 
                                                onmouseover=\"this.src='";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Home_Selected.jpg"), "html", null, true);
        echo "';\"
                                                onmouseout =\"this.src='";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Home.jpg"), "html", null, true);
        echo "';\"></a></li
                                ><li class=\"Gallery\"><a class=\"Gallery\" href=\"Gallery\">
                                        <img src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Gallery.jpg"), "html", null, true);
        echo "\"
                                                accesskey=\"\" 
                                                onmouseover=\"this.src='";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Gallery_Selected.jpg"), "html", null, true);
        echo "';\"
                                                onmouseout =\"this.src='";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Gallery.jpg"), "html", null, true);
        echo "';\"></a></li
                                ><li class=\"Forum\"><a class=\"Forum\" href=\"Forum\">
                                        <img src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Forum.jpg"), "html", null, true);
        echo "\"
                                                accesskey=\"\" 
                                                onmouseover=\"this.src='";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Forum_Selected.jpg"), "html", null, true);
        echo "';\"
                                                onmouseout =\"this.src='";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Forum.jpg"), "html", null, true);
        echo "';\"></a></li
                                ><li class=\"Contact\"><a class=\"Contact\" href=\"Contact\">
                                        <img src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Contact.jpg"), "html", null, true);
        echo "\"
                                                accesskey=\"\" 
                                                onmouseover=\"this.src='";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Contact_Selected.jpg"), "html", null, true);
        echo "';\"
                                                onmouseout =\"this.src='";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Contact.jpg"), "html", null, true);
        echo "';\"></a></li
                                ></li><img class=\"header_bas_Gauche\"src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/header_bas_droit.jpg"), "html", null, true);
        echo "\" 
                            >
                            
                </div>
            </div>
            <div name=\"body\">
                <div name=\"tbody\">
                    ";
        // line 67
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 20));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 68
            echo "                        <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/body_top.jpg"), "html", null, true);
            echo "\"
                        ><br>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "                </div>
            </div>
            <div name=\"footer\">
                <img src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/footer_gauche.jpg"), "html", null, true);
        echo "\"
                ><img src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/footer_centre.jpg"), "html", null, true);
        echo "\"
                ><img src=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/footer_droit.jpg"), "html", null, true);
        echo "\"
                ><img src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Footer_Footer.jpg"), "html", null, true);
        echo "\"
                ><br>
            </div>
    </center>   
</body>
    
</html>";
    }

    public function getTemplateName()
    {
        return "CarsGalleryBlogBundle:Blog:Gallery.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 77,  209 => 76,  205 => 75,  201 => 74,  196 => 71,  186 => 68,  182 => 67,  172 => 60,  168 => 59,  164 => 58,  159 => 56,  154 => 54,  150 => 53,  145 => 51,  140 => 49,  136 => 48,  131 => 46,  126 => 44,  122 => 43,  117 => 41,  112 => 39,  105 => 35,  98 => 31,  94 => 30,  90 => 29,  82 => 24,  78 => 23,  74 => 22,  65 => 15,  47 => 11,  43 => 10,  29 => 8,  25 => 5,  19 => 1,);
    }
}
