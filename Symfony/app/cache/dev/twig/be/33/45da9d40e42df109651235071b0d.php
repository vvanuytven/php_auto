<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_be3345da9d40e42df109651235071b0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("@WebProfiler/Profiler/base.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "    <div id=\"content\">
        ";
        // line 5
        $this->env->loadTemplate("@WebProfiler/Profiler/header.html.twig")->display(array());
        // line 6
        echo "
        <div id=\"main\">
            <div class=\"clear-fix\">
                <div id=\"collector-wrapper\">
                    <div id=\"collector-content\">
                        ";
        // line 11
        $this->displayBlock('panel', $context, $blocks);
        // line 34
        echo "                    </div>
                </div>
                <div id=\"navigation\">
                    ";
        // line 37
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_search_bar"));
        echo "
                    ";
        // line 38
        $this->env->loadTemplate("@WebProfiler/Profiler/admin.html.twig")->display(array("token" => ""));
        // line 39
        echo "                </div>
            </div>
        </div>
    </div>
";
    }

    // line 11
    public function block_panel($context, array $blocks = array())
    {
        // line 12
        echo "                            ";
        if (((isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")) == "purge")) {
            // line 13
            echo "                                <h2>The profiler database was purged successfully</h2>
                                <p>
                                    <em>Now you need to browse some pages with the Symfony Profiler enabled to collect data.</em>
                                </p>
                            ";
        } elseif (((isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")) == "upload_error")) {
            // line 18
            echo "                                <h2>A problem occurred when uploading the data</h2>
                                <p>
                                    <em>No file given or the file was not uploaded successfully.</em>
                                </p>
                            ";
        } elseif (((isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")) == "already_exists")) {
            // line 23
            echo "                                <h2>A problem occurred when uploading the data</h2>
                                <p>
                                    <em>The token already exists in the database.</em>
                                </p>
                            ";
        } elseif (((isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")) == "no_token")) {
            // line 28
            echo "                                <h2>Token not found</h2>
                                <p>
                                    <em>Token \"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
            echo "\" was not found in the database.</em>
                                </p>
                            ";
        }
        // line 33
        echo "                        ";
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 33,  81 => 29,  77 => 28,  53 => 15,  250 => 86,  242 => 84,  238 => 83,  232 => 80,  227 => 78,  223 => 77,  219 => 76,  215 => 75,  210 => 73,  204 => 70,  195 => 64,  191 => 63,  113 => 33,  97 => 26,  59 => 11,  34 => 11,  213 => 77,  205 => 75,  186 => 68,  172 => 60,  159 => 52,  150 => 53,  90 => 29,  65 => 11,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 56,  140 => 45,  132 => 51,  128 => 39,  119 => 42,  107 => 36,  71 => 13,  177 => 58,  165 => 64,  160 => 61,  135 => 43,  126 => 44,  114 => 42,  84 => 35,  70 => 26,  67 => 15,  61 => 13,  94 => 38,  89 => 20,  85 => 23,  75 => 28,  68 => 12,  56 => 16,  201 => 74,  196 => 71,  183 => 70,  171 => 61,  166 => 71,  163 => 53,  158 => 67,  156 => 58,  151 => 57,  142 => 59,  138 => 57,  136 => 48,  121 => 35,  117 => 34,  105 => 28,  91 => 37,  62 => 24,  49 => 14,  38 => 6,  26 => 3,  87 => 20,  31 => 5,  25 => 5,  28 => 3,  21 => 2,  24 => 3,  19 => 1,  93 => 28,  88 => 19,  78 => 18,  46 => 34,  44 => 11,  27 => 4,  79 => 29,  72 => 16,  69 => 25,  47 => 11,  40 => 7,  37 => 6,  22 => 2,  246 => 85,  157 => 56,  145 => 47,  139 => 50,  131 => 46,  123 => 47,  120 => 40,  115 => 43,  111 => 37,  108 => 37,  101 => 27,  98 => 31,  96 => 30,  83 => 30,  74 => 22,  66 => 25,  55 => 38,  52 => 21,  50 => 15,  43 => 11,  41 => 10,  35 => 5,  32 => 4,  29 => 3,  209 => 76,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 62,  182 => 60,  176 => 64,  173 => 57,  168 => 55,  164 => 58,  162 => 62,  154 => 50,  149 => 48,  147 => 58,  144 => 53,  141 => 51,  133 => 55,  130 => 41,  125 => 44,  122 => 43,  116 => 36,  112 => 39,  109 => 47,  106 => 36,  103 => 37,  99 => 30,  95 => 34,  92 => 28,  86 => 28,  82 => 24,  80 => 19,  73 => 27,  64 => 17,  60 => 6,  57 => 39,  54 => 10,  51 => 37,  48 => 13,  45 => 17,  42 => 8,  39 => 10,  36 => 5,  33 => 4,  30 => 5,);
    }
}
