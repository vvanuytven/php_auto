<?php

/* WebProfilerBundle:Profiler:bag.html.twig */
class __TwigTemplate_dd8fd04247b853d1aa6e383ec340a062 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table ";
        if (array_key_exists("class", $context)) {
            echo "class='";
            echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")), "html", null, true);
            echo "'";
        }
        echo " >
    <thead>
        <tr>
            <th scope=\"col\">Key</th>
            <th scope=\"col\">Value</th>
        </tr>
    </thead>
    <tbody>
        ";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(twig_sort_filter($this->getAttribute((isset($context["bag"]) ? $context["bag"] : $this->getContext($context, "bag")), "keys")));
        foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
            // line 10
            echo "            <tr>
                <th>";
            // line 11
            echo twig_escape_filter($this->env, (isset($context["key"]) ? $context["key"] : $this->getContext($context, "key")), "html", null, true);
            echo "</th>
                ";
            // line 13
            echo "                <td>";
            echo twig_escape_filter($this->env, twig_jsonencode_filter($this->getAttribute((isset($context["bag"]) ? $context["bag"] : $this->getContext($context, "bag")), "get", array(0 => (isset($context["key"]) ? $context["key"] : $this->getContext($context, "key"))), "method"), (64 | 256)), "html", null, true);
            echo "</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "    </tbody>
</table>
";
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:bag.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 21,  356 => 328,  344 => 318,  342 => 317,  339 => 316,  297 => 276,  295 => 275,  102 => 33,  81 => 29,  77 => 28,  53 => 15,  250 => 86,  242 => 84,  238 => 83,  232 => 80,  227 => 78,  223 => 77,  219 => 76,  215 => 75,  210 => 73,  204 => 70,  195 => 64,  191 => 63,  113 => 33,  97 => 26,  59 => 11,  34 => 11,  213 => 77,  205 => 75,  186 => 68,  172 => 60,  159 => 52,  150 => 53,  90 => 29,  65 => 11,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 56,  140 => 45,  132 => 51,  128 => 39,  119 => 42,  107 => 36,  71 => 23,  177 => 58,  165 => 64,  160 => 61,  135 => 43,  126 => 44,  114 => 42,  84 => 27,  70 => 26,  67 => 22,  61 => 13,  94 => 38,  89 => 30,  85 => 23,  75 => 24,  68 => 12,  56 => 16,  201 => 74,  196 => 71,  183 => 70,  171 => 61,  166 => 71,  163 => 53,  158 => 67,  156 => 58,  151 => 57,  142 => 59,  138 => 57,  136 => 48,  121 => 35,  117 => 34,  105 => 28,  91 => 37,  62 => 24,  49 => 14,  38 => 6,  26 => 3,  87 => 20,  31 => 4,  25 => 5,  28 => 3,  21 => 2,  24 => 3,  19 => 1,  93 => 28,  88 => 19,  78 => 18,  46 => 13,  44 => 11,  27 => 7,  79 => 29,  72 => 16,  69 => 25,  47 => 11,  40 => 8,  37 => 7,  22 => 2,  246 => 85,  157 => 56,  145 => 47,  139 => 50,  131 => 46,  123 => 47,  120 => 40,  115 => 43,  111 => 37,  108 => 37,  101 => 27,  98 => 31,  96 => 30,  83 => 30,  74 => 22,  66 => 25,  55 => 38,  52 => 21,  50 => 18,  43 => 11,  41 => 10,  35 => 9,  32 => 4,  29 => 3,  209 => 76,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 62,  182 => 60,  176 => 64,  173 => 57,  168 => 55,  164 => 58,  162 => 62,  154 => 50,  149 => 48,  147 => 58,  144 => 53,  141 => 51,  133 => 55,  130 => 41,  125 => 44,  122 => 43,  116 => 36,  112 => 39,  109 => 47,  106 => 36,  103 => 37,  99 => 30,  95 => 34,  92 => 28,  86 => 28,  82 => 24,  80 => 19,  73 => 27,  64 => 17,  60 => 6,  57 => 20,  54 => 19,  51 => 37,  48 => 16,  45 => 17,  42 => 11,  39 => 10,  36 => 7,  33 => 6,  30 => 5,);
    }
}
