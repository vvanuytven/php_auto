<?php

/* WebProfilerBundle:Collector:exception.css.twig */
class __TwigTemplate_91abbe73e27ef2a05f8f96c35e1d835b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo ".sf-reset .traces {
    padding-bottom: 14px;
}
.sf-reset .traces li {
    font-size: 12px;
    color: #868686;
    padding: 5px 4px;
    list-style-type: decimal;
    margin-left: 20px;
    white-space: break-word;
}
.sf-reset #logs .traces li.error {
    font-style: normal;
    color: #AA3333;
    background: #f9ecec;
}
.sf-reset #logs .traces li.warning {
    font-style: normal;
    background: #ffcc00;
}
/* fix for Opera not liking empty <li> */
.sf-reset .traces li:after {
    content: \"\\00A0\";
}
.sf-reset .trace {
    border: 1px solid #D3D3D3;
    padding: 10px;
    overflow: auto;
    margin: 10px 0 20px;
}
.sf-reset .block-exception {
    border-radius: 16px;
    margin-bottom: 20px;
    background-color: #f6f6f6;
    border: 1px solid #dfdfdf;
    padding: 30px 28px;
    word-wrap: break-word;
    overflow: hidden;
}
.sf-reset .block-exception div {
    color: #313131;
    font-size: 10px;
}
.sf-reset .block-exception-detected .illustration-exception,
.sf-reset .block-exception-detected .text-exception {
    float: left;
}
.sf-reset .block-exception-detected .illustration-exception {
    width: 152px;
}
.sf-reset .block-exception-detected .text-exception {
    width: 670px;
    padding: 30px 44px 24px 46px;
    position: relative;
}
.sf-reset .text-exception .open-quote,
.sf-reset .text-exception .close-quote {
    position: absolute;
}
.sf-reset .open-quote {
    top: 0;
    left: 0;
}
.sf-reset .close-quote {
    bottom: 0;
    right: 50px;
}
.sf-reset .block-exception p {
    font-family: Arial, Helvetica, sans-serif;
}
.sf-reset .block-exception p a,
.sf-reset .block-exception p a:hover {
    color: #565656;
}
.sf-reset .logs h2 {
    float: left;
    width: 654px;
}
.sf-reset .error-count {
    float: right;
    width: 170px;
    text-align: right;
}
.sf-reset .error-count span {
    display: inline-block;
    background-color: #aacd4e;
    border-radius: 6px;
    padding: 4px;
    color: white;
    margin-right: 2px;
    font-size: 11px;
    font-weight: bold;
}
.sf-reset .toggle {
    vertical-align: middle;
}
.sf-reset .linked ul,
.sf-reset .linked li {
    display: inline;
}
.sf-reset #output-content {
    color: #000;
    font-size: 12px;
}
";
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.css.twig";
    }

    public function getDebugInfo()
    {
        return array (  806 => 488,  803 => 487,  792 => 485,  788 => 484,  784 => 482,  771 => 481,  745 => 476,  742 => 475,  723 => 473,  706 => 472,  702 => 470,  698 => 469,  694 => 468,  690 => 467,  686 => 466,  682 => 465,  678 => 464,  675 => 463,  673 => 462,  656 => 461,  645 => 460,  630 => 455,  625 => 453,  621 => 452,  618 => 451,  616 => 450,  602 => 449,  565 => 414,  547 => 411,  530 => 410,  527 => 409,  525 => 408,  520 => 406,  515 => 404,  244 => 136,  188 => 90,  357 => 123,  332 => 116,  327 => 114,  324 => 113,  318 => 111,  306 => 107,  291 => 102,  263 => 95,  243 => 92,  231 => 83,  212 => 78,  202 => 77,  190 => 76,  174 => 65,  161 => 63,  462 => 202,  449 => 198,  446 => 197,  441 => 196,  439 => 195,  431 => 189,  429 => 188,  422 => 184,  415 => 180,  408 => 176,  401 => 172,  394 => 168,  373 => 156,  351 => 120,  348 => 140,  338 => 135,  335 => 134,  329 => 131,  325 => 129,  323 => 128,  320 => 127,  303 => 106,  300 => 105,  289 => 113,  286 => 112,  275 => 105,  270 => 102,  267 => 101,  262 => 98,  256 => 96,  233 => 87,  226 => 84,  207 => 75,  200 => 72,  185 => 74,  181 => 65,  153 => 77,  389 => 160,  386 => 159,  380 => 160,  378 => 157,  371 => 156,  367 => 155,  363 => 126,  361 => 146,  358 => 151,  353 => 121,  345 => 147,  343 => 146,  340 => 145,  334 => 141,  331 => 140,  328 => 139,  326 => 138,  321 => 112,  315 => 110,  307 => 128,  302 => 125,  296 => 121,  293 => 120,  290 => 119,  288 => 101,  281 => 114,  276 => 111,  274 => 97,  269 => 107,  265 => 96,  259 => 103,  255 => 93,  253 => 100,  248 => 94,  222 => 83,  216 => 79,  197 => 71,  194 => 70,  184 => 63,  178 => 64,  175 => 58,  170 => 84,  155 => 47,  152 => 46,  134 => 47,  127 => 35,  104 => 32,  100 => 36,  76 => 34,  58 => 25,  20 => 1,  63 => 21,  356 => 328,  344 => 119,  342 => 137,  339 => 316,  297 => 104,  295 => 275,  102 => 33,  81 => 23,  77 => 25,  53 => 12,  250 => 86,  242 => 84,  238 => 83,  232 => 88,  227 => 86,  223 => 77,  219 => 76,  215 => 75,  210 => 77,  204 => 70,  195 => 64,  191 => 69,  113 => 38,  97 => 26,  59 => 14,  34 => 11,  213 => 78,  205 => 75,  186 => 68,  172 => 62,  159 => 52,  150 => 55,  90 => 42,  65 => 22,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 199,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 164,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 143,  341 => 118,  337 => 103,  322 => 101,  314 => 99,  312 => 109,  309 => 108,  305 => 95,  298 => 120,  294 => 90,  285 => 89,  283 => 100,  278 => 98,  268 => 85,  264 => 84,  258 => 94,  252 => 80,  247 => 78,  241 => 90,  235 => 85,  229 => 85,  224 => 81,  220 => 81,  214 => 69,  208 => 76,  169 => 60,  143 => 51,  140 => 45,  132 => 51,  128 => 39,  119 => 40,  107 => 36,  71 => 23,  177 => 58,  165 => 83,  160 => 61,  135 => 43,  126 => 44,  114 => 42,  84 => 40,  70 => 19,  67 => 18,  61 => 15,  94 => 21,  89 => 30,  85 => 24,  75 => 19,  68 => 30,  56 => 16,  201 => 74,  196 => 92,  183 => 70,  171 => 61,  166 => 54,  163 => 82,  158 => 80,  156 => 62,  151 => 57,  142 => 59,  138 => 57,  136 => 71,  121 => 35,  117 => 39,  105 => 34,  91 => 34,  62 => 27,  49 => 11,  38 => 18,  26 => 6,  87 => 41,  31 => 8,  25 => 5,  28 => 3,  21 => 2,  24 => 3,  19 => 1,  93 => 28,  88 => 25,  78 => 18,  46 => 10,  44 => 20,  27 => 3,  79 => 21,  72 => 18,  69 => 17,  47 => 21,  40 => 11,  37 => 7,  22 => 2,  246 => 93,  157 => 56,  145 => 74,  139 => 49,  131 => 45,  123 => 61,  120 => 31,  115 => 43,  111 => 37,  108 => 37,  101 => 31,  98 => 45,  96 => 30,  83 => 31,  74 => 22,  66 => 25,  55 => 24,  52 => 12,  50 => 22,  43 => 9,  41 => 19,  35 => 6,  32 => 6,  29 => 3,  209 => 76,  203 => 73,  199 => 93,  193 => 73,  189 => 66,  187 => 75,  182 => 87,  176 => 86,  173 => 85,  168 => 61,  164 => 58,  162 => 59,  154 => 50,  149 => 48,  147 => 75,  144 => 42,  141 => 73,  133 => 55,  130 => 46,  125 => 42,  122 => 41,  116 => 57,  112 => 36,  109 => 52,  106 => 51,  103 => 37,  99 => 31,  95 => 34,  92 => 43,  86 => 28,  82 => 19,  80 => 19,  73 => 33,  64 => 17,  60 => 20,  57 => 19,  54 => 19,  51 => 37,  48 => 16,  45 => 10,  42 => 7,  39 => 6,  36 => 5,  33 => 4,  30 => 5,);
    }
}
