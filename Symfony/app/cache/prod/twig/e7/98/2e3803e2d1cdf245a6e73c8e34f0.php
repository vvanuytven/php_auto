<?php

/* ::base.html.twig */
class __TwigTemplate_e7982e3803e2d1cdf245a6e73c8e34f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 6,  23 => 1,  234 => 82,  230 => 81,  226 => 80,  222 => 79,  217 => 77,  190 => 68,  181 => 62,  83 => 24,  53 => 5,  213 => 76,  205 => 74,  186 => 68,  172 => 60,  159 => 55,  150 => 53,  145 => 50,  131 => 45,  90 => 29,  74 => 22,  65 => 15,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 56,  140 => 48,  132 => 51,  128 => 49,  119 => 42,  111 => 37,  107 => 33,  71 => 19,  177 => 61,  165 => 64,  160 => 61,  139 => 50,  135 => 46,  126 => 43,  114 => 37,  84 => 28,  70 => 20,  67 => 15,  61 => 13,  47 => 12,  94 => 30,  89 => 20,  85 => 25,  79 => 18,  75 => 18,  68 => 14,  56 => 9,  50 => 12,  201 => 73,  196 => 71,  183 => 70,  171 => 61,  166 => 71,  163 => 56,  158 => 67,  156 => 58,  151 => 57,  142 => 59,  138 => 57,  136 => 48,  123 => 47,  121 => 41,  117 => 41,  115 => 43,  105 => 35,  101 => 32,  91 => 26,  69 => 11,  62 => 23,  49 => 19,  38 => 6,  26 => 6,  87 => 25,  72 => 16,  66 => 24,  55 => 15,  31 => 5,  25 => 5,  43 => 10,  41 => 7,  28 => 3,  35 => 7,  29 => 5,  21 => 2,  24 => 3,  19 => 1,  98 => 31,  93 => 28,  88 => 6,  78 => 23,  46 => 7,  44 => 11,  40 => 7,  32 => 4,  27 => 4,  22 => 2,  209 => 75,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 67,  176 => 64,  173 => 60,  168 => 58,  164 => 58,  162 => 62,  154 => 53,  149 => 51,  147 => 58,  144 => 53,  141 => 51,  133 => 55,  130 => 41,  125 => 44,  122 => 43,  116 => 36,  112 => 39,  109 => 41,  106 => 33,  103 => 32,  99 => 31,  95 => 34,  92 => 33,  86 => 28,  82 => 24,  80 => 19,  73 => 19,  64 => 10,  60 => 13,  57 => 14,  54 => 10,  51 => 14,  48 => 13,  45 => 8,  42 => 10,  39 => 9,  36 => 5,  33 => 6,  30 => 10,);
    }
}
