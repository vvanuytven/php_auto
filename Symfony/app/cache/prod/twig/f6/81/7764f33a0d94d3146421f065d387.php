<?php

/* CarsGalleryBlogBundle:Blog:Home.html.twig */
class __TwigTemplate_f6817764f33a0d94d3146421f065d387 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
<title>Cars Gallery</title>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">

";
        // line 6
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "8822ae8_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_8822ae8_0") : $this->env->getExtension('assets')->getAssetUrl("css/8822ae8_Main_1.css");
            // line 10
            echo "<link type=\"text/css\" rel=\"stylesheet\" media=\"all\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
            // asset "8822ae8_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_8822ae8_1") : $this->env->getExtension('assets')->getAssetUrl("css/8822ae8_Home_2.css");
            echo "<link type=\"text/css\" rel=\"stylesheet\" media=\"all\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
        } else {
            // asset "8822ae8"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_8822ae8") : $this->env->getExtension('assets')->getAssetUrl("css/8822ae8.css");
            echo "<link type=\"text/css\" rel=\"stylesheet\" media=\"all\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
        }
        unset($context["asset_url"]);
        // line 12
        echo "
";
        // line 13
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "5890f52_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_5890f52_0") : $this->env->getExtension('assets')->getAssetUrl("js/5890f52_main_1.js");
            // line 14
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    <script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js\"></script>
    <script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js\"></script>
";
        } else {
            // asset "5890f52"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_5890f52") : $this->env->getExtension('assets')->getAssetUrl("js/5890f52.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    <script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js\"></script>
    <script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js\"></script>
";
        }
        unset($context["asset_url"]);
        // line 18
        echo "</head>

<body class=\"bgimage\" alt=\"\" onload=\"Slider();\">
<!-- Save for Web Slices (MyPsd.psd) -->
<center>
                <div name=\"theader\">
                            <img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/theader_gauche.jpg"), "html", null, true);
        echo "\"
                            ><img src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/theader_rss.jpg"), "html", null, true);
        echo "\"
                            ><img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/theader_droit.jpg"), "html", null, true);
        echo "\"
                            ><br>
                </div>

                <div name=\"header\" class=\"header\">
                            <img src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/header_top_gauche.jpg"), "html", null, true);
        echo "\" 
                            ><img src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/header_top_Logo.jpg"), "html", null, true);
        echo "\"
                            ><img src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/header_top_centre.jpg"), "html", null, true);
        echo "\"
                            ><form
                            ><input class=\"searchBox\" type=\"text\" value=\"Search your car !! \" onclick=\"this.value=''\" onmouseout=\"this.value='Search your car!!'\"
                            ><input class=\"searchBtn\" type=\"submit\" value=\"\"
                            ><img src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/header_top_droit.jpg"), "html", null, true);
        echo "\"</form><br>

                </div>
                <div name=\"bheader\">
                            <img class=\"header_bas_Gauche\"src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/header_bas_gauche.jpg"), "html", null, true);
        echo "\"
                            ><li class=\"Home\"><a class=\"Home\" href=\"Home\">
                                        <img src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Home.jpg"), "html", null, true);
        echo "\" 
                                                accesskey=\"\" 
                                                onmouseover=\"this.src='";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Home_Selected.jpg"), "html", null, true);
        echo "';\"
                                                onmouseout =\"this.src='";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Home.jpg"), "html", null, true);
        echo "';\"></a></li
                                ><li class=\"Gallery\"><a class=\"Gallery\" href=\"Gallery\">
                                        <img src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Gallery.jpg"), "html", null, true);
        echo "\"
                                                accesskey=\"\" 
                                                onmouseover=\"this.src='";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Gallery_Selected.jpg"), "html", null, true);
        echo "';\"
                                                onmouseout =\"this.src='";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Gallery.jpg"), "html", null, true);
        echo "';\"></a></li
                                ><li class=\"Forum\"><a class=\"Forum\" href=\"Forum\">
                                        <img src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Forum.jpg"), "html", null, true);
        echo "\"
                                                accesskey=\"\" 
                                                onmouseover=\"this.src='";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Forum_Selected.jpg"), "html", null, true);
        echo "';\"
                                                onmouseout =\"this.src='";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Forum.jpg"), "html", null, true);
        echo "';\"></a></li
                                ><li class=\"Contact\"><a class=\"Contact\" href=\"Contact\">
                                        <img src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Contact.jpg"), "html", null, true);
        echo "\"
                                                accesskey=\"\" 
                                                onmouseover=\"this.src='";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Contact_Selected.jpg"), "html", null, true);
        echo "';\"
                                                onmouseout =\"this.src='";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Menu_Contact.jpg"), "html", null, true);
        echo "';\"></a></li
                                ></li><img class=\"header_bas_Gauche\"src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/header_bas_droit.jpg"), "html", null, true);
        echo "\" 
                            >
                            
                
            </div>
            <div name=\"tbody\">
                <img src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/body_top.jpg"), "html", null, true);
        echo "\"
                ><br>
            </div
            ><img src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/body_centre_gauche.jpg"), "html", null, true);
        echo "\"
            ><span id=\"slide\">
                            <img id=\"1\" class=\"slider\" src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/slider/img.jpg"), "html", null, true);
        echo "\">
                            <img id=\"2\" class=\"slider\" src=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/slider/img2.jpg"), "html", null, true);
        echo "\">
                            <img id=\"3\" class=\"slider\" src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/slider/img3.jpg"), "html", null, true);
        echo "\">
            </span><img src=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/body_centre_droit.jpg"), "html", null, true);
        echo "\"
            ><img src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/body_body.jpg"), "html", null, true);
        echo "\">
            <div name=\"footer\">
                <img src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/footer_gauche.jpg"), "html", null, true);
        echo "\"
                ><img src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/footer_centre.jpg"), "html", null, true);
        echo "\"
                ><img src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/footer_droit.jpg"), "html", null, true);
        echo "\"
                ><img src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/carsgalleryblog/images/Footer_Footer.jpg"), "html", null, true);
        echo "\"
                ><br>
            </div>
    </center>   
</body>
    
</html>";
    }

    public function getTemplateName()
    {
        return "CarsGalleryBlogBundle:Blog:Home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 82,  230 => 81,  226 => 80,  222 => 79,  217 => 77,  190 => 68,  181 => 62,  83 => 24,  53 => 13,  213 => 76,  205 => 74,  186 => 68,  172 => 60,  159 => 55,  150 => 53,  145 => 50,  131 => 45,  90 => 29,  74 => 22,  65 => 15,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 60,  143 => 56,  140 => 48,  132 => 51,  128 => 49,  119 => 42,  111 => 37,  107 => 33,  71 => 19,  177 => 61,  165 => 64,  160 => 61,  139 => 50,  135 => 46,  126 => 43,  114 => 37,  84 => 28,  70 => 20,  67 => 15,  61 => 13,  47 => 11,  94 => 30,  89 => 20,  85 => 25,  79 => 18,  75 => 18,  68 => 14,  56 => 9,  50 => 12,  201 => 73,  196 => 71,  183 => 70,  171 => 61,  166 => 71,  163 => 56,  158 => 67,  156 => 58,  151 => 57,  142 => 59,  138 => 57,  136 => 48,  123 => 47,  121 => 41,  117 => 41,  115 => 43,  105 => 35,  101 => 32,  91 => 26,  69 => 25,  62 => 23,  49 => 19,  38 => 6,  26 => 6,  87 => 25,  72 => 16,  66 => 24,  55 => 15,  31 => 5,  25 => 5,  43 => 10,  41 => 7,  28 => 3,  35 => 5,  29 => 8,  21 => 2,  24 => 3,  19 => 1,  98 => 31,  93 => 28,  88 => 6,  78 => 23,  46 => 7,  44 => 12,  40 => 7,  32 => 4,  27 => 4,  22 => 2,  209 => 75,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 67,  176 => 64,  173 => 60,  168 => 58,  164 => 58,  162 => 62,  154 => 53,  149 => 51,  147 => 58,  144 => 53,  141 => 51,  133 => 55,  130 => 41,  125 => 44,  122 => 43,  116 => 36,  112 => 39,  109 => 41,  106 => 33,  103 => 32,  99 => 31,  95 => 34,  92 => 33,  86 => 28,  82 => 24,  80 => 19,  73 => 19,  64 => 17,  60 => 13,  57 => 14,  54 => 10,  51 => 14,  48 => 13,  45 => 8,  42 => 7,  39 => 9,  36 => 5,  33 => 4,  30 => 10,);
    }
}
