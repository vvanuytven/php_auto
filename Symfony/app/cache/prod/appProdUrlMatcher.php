<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        // cars_gallery_blog_index
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'cars_gallery_blog_index');
            }

            return array (  '_controller' => 'CarsGallery\\BlogBundle\\Controller\\BlogController::indexAction',  '_route' => 'cars_gallery_blog_index',);
        }

        // cars_gallery_blog_homepage
        if ($pathinfo === '/Home') {
            return array (  '_controller' => 'CarsGallery\\BlogBundle\\Controller\\BlogController::indexAction',  '_route' => 'cars_gallery_blog_homepage',);
        }

        // cars_gallery_blog_gallerypage
        if ($pathinfo === '/Gallery') {
            return array (  '_controller' => 'CarsGallery\\BlogBundle\\Controller\\BlogController::GalleryAction',  '_route' => 'cars_gallery_blog_gallerypage',);
        }

        // cars_gallery_blog_forumpage
        if ($pathinfo === '/Forum') {
            return array (  '_controller' => 'CarsGallery\\BlogBundle\\Controller\\BlogController::ForumAction',  '_route' => 'cars_gallery_blog_forumpage',);
        }

        // cars_gallery_blog_contactpage
        if ($pathinfo === '/Contact') {
            return array (  '_controller' => 'CarsGallery\\BlogBundle\\Controller\\BlogController::ContactAction',  '_route' => 'cars_gallery_blog_contactpage',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
